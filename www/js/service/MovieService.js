app.factory('movieService', ['$http', '$location', function($http, $location) {

    return {
        getPopular: function() {
            return $http.get(url_path+'movie/popular'+'?api_key='+api_key+'&language=fr');
        },
        getNowPlaying: function() {
            return $http.get(url_path+'movie/now_playing'+'?api_key='+api_key+'&language=fr');
        },
        getSearch: function(query) {
            return $http.get(url_path+'search/movie'+'?api_key='+api_key+'&query='+query+'&language=fr');
        },
        getOne: function (id) {
            return $http.get(url_path+'movie/'+id+'?api_key='+api_key+'&language=fr');
        },
        getVideo: function (id) {
            return $http.get(url_path+'movie/'+id+'/videos'+'?api_key='+api_key);
        },
        getCredits: function (id) {
            return $http.get(url_path+'movie/'+id+'/credits'+'?api_key='+api_key);
        },
        getActor: function (id) {
            return $http.get(url_path+'person/'+id+'?api_key='+api_key);
        },
        getActorMovieCredits: function (id) {
            return $http.get(url_path+'person/'+id+'/movie_credits'+'?api_key='+api_key);
        },
        getConfiguration: function () {
            return $http.get(url_path+'configuration'+'?api_key='+api_key);
        }
    }
}]);