var url_path = "https://api.themoviedb.org/3/";
var api_key = "e3edcd7953bcba4289082fca1ea4cc1f";

var app = angular.module('app', ['ngRoute', 'ngTouch', 'ngSanitize', 'ngAnimate']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {templateUrl: 'template/home.html'})
        .when('/now-playing', {templateUrl: 'template/now.html'})
        .when('/geolocate', {templateUrl: 'template/geolocate.html'})
        .when('/popular', {templateUrl: 'template/popular.html'})
        .when('/search', {templateUrl: 'template/search.html'})
        .when('/movie/:id', {templateUrl: 'template/movie-detail.html', controller: 'MovieDetailCtrl'})
        .when('/actor/:id', {templateUrl: 'template/actor-detail.html', controller: 'ActorDetailCtrl'})
        .otherwise({redirectTo: '/'})
}]);

app.run(['$rootScope', '$location', function ($rootScope, $location) {
    $rootScope.$on('$routeChangeStart', function (a, b) {
        $rootScope.routeChange = b;
    });

    $rootScope.gutter = 15;
    $rootScope.width = window.innerWidth - (2 * $rootScope.gutter);
    $rootScope.height = window.innerWidth / 2;

    $rootScope.previous = function () {
        window.history.back();
    };

    $(window).resize(function () {
        $('iframe').each(function () {
            $(this).css({
                'height': window.innerWidth / 2,
                'width': window.innerWidth - (2 * $rootScope.gutter)
            });
        });
        $rootScope.width = window.innerWidth - (2 * $rootScope.gutter);
        $rootScope.height = window.innerWidth / 2;

        $('#map').css({
            height: window.innerHeight - 48,
            width: window.innerWidth
        });
    });
}]);

app.directive('loading', function () {
    return {
        templateUrl:'template/loading.html'
    }
});

app.directive('actor', function () {
    return {
        templateUrl:'template/actor.html'
    }
});

app.directive('movie', function () {
    return {
        templateUrl:'template/movie.html'
    }
});

app.filter('range', function() {
    return function(input) {
        var lowBound, highBound;
        switch (input.length) {
            case 1:
                lowBound = 0;
                highBound = parseInt(input[0]) - 1;
                break;
            case 2:
                lowBound = parseInt(input[0]);
                highBound = parseInt(input[1]);
                break;
            default:
                return input;
        }
        var result = [];
        for (var i = lowBound; i <= highBound; i++)
            result.push(i);
        return result;
    };
});

app.controller('AppCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams) {
    $rootScope.title = "ng-movies";
}]);

app.controller('HomeCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams) {

    $scope.nowPlaying = function () {
        $location.path('/now-playing');
    };

    $scope.geolocate = function () {
        $location.path('/geolocate');
    };

    $scope.popular = function () {
        $location.path('/popular');
    };

    $scope.search = function () {
        $location.path('/search');
    };

}]);

app.controller('NowCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams) {

    $scope.loading = true;

    movieService.getConfiguration()
        .success(function (result) {
            var base_url = result.images.base_url;
            movieService.getNowPlaying().success(function (results) {
                for (var i = 0; i < results.results.length; i++) {
                    if(results.results[i].poster_path) {
                        results.results[i].poster = base_url + "w300" + results.results[i].poster_path;
                    } else {
                        results.results[i].poster = "img/movie-default.png";
                    }
                }
                $scope.results = results.results;
            });
        })
        .then(function () {
            $scope.loading = false;
        });

    $scope.detail = function (id) {
        $location.path('/movie/' + id);
    };

}]);

app.controller('PopularCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams) {

    $scope.loading = true;

    movieService.getConfiguration()
        .success(function (result) {
            var base_url = result.images.base_url;
            movieService.getPopular().success(function (results) {
                for (var i = 0; i < results.results.length; i++) {
                    if(results.results[i].poster_path) {
                        results.results[i].poster = base_url + "w300" + results.results[i].poster_path;
                    } else {
                        results.results[i].poster = "img/movie-default.png";
                    }
                }
                $scope.results = results.results;
            })
            .then(function () {
                $scope.loading = false;
            });
        });

    $scope.detail = function (id) {
        $location.path('/movie/' + id);
    };

}]);

app.controller('SearchCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams) {

    $scope.launchSearch = function () {

        $scope.loading = true;

        movieService.getConfiguration()
            .success(function (result) {
                var base_url = result.images.base_url;
                movieService.getSearch($scope.query)
                    .success(function (results) {

                        for (var i = 0; i < results.results.length; i++) {
                            if(results.results[i].poster_path) {
                                results.results[i].poster = base_url + "w300" + results.results[i].poster_path;
                            } else {
                                results.results[i].poster = "img/movie-default.png";
                            }
                        }

                        $scope.results = results.results;
                    });
            })
            .then(function () {
                $scope.loading = false;
            });
    };

    $scope.detail = function (id) {
        $location.path('/movie/' + id);
    };

}]);

app.controller('GeolocateCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams) {

    function loadTheaters() {
        var url = "theaters.json";
        return $http.get(url);
    }

    var onSuccess = function (position) {

        $('#map').css({
            height: window.innerHeight - 48,
            width: window.innerWidth
        });

        loadTheaters().success(function (data) {

            var map = L.map('map').setView([position.coords.latitude, position.coords.longitude], 14);

            L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',
                id: 'examples.map-i875mjb7'
            }).addTo(map);


            L.marker([position.coords.latitude, position.coords.longitude]).addTo(map)
                .bindPopup("<b>Vous êtes ici !</b>").openPopup();

            for (var i = 0; i < data.theaters.length; i++) {
                L.marker([data.theaters[i].latitude, data.theaters[i].longitude]).addTo(map).bindPopup(data.theaters[i].name).on('click', function () {
                    this.openPopup();
                });
            }
        });
    };

    function onError(error) {
        alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);

}]);

app.controller('MovieDetailCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', '$sce', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams, $sce) {

    var base_url = "http://image.tmdb.org/t/p/";

    $scope.loading = true;

    $scope.actorLimit = 6;

    $scope.videoLimit = 1;

    $scope.crewLimit = 4;

    movieService.getOne($routeParams.id)
        .success(function (result) {

            if(result.poster_path) {
                result.poster = base_url + "w300" + result.poster_path;
            } else {
                result.poster = "img/movie-default.png";
            }

            $scope.movie = result;

            $scope.movie.grade = Math.round($scope.movie.vote_average/2);
        })
        .then(function () {
            $scope.loading = false;
        });

    movieService.getVideo($routeParams.id)
        .success(function (result) {
            var videos = result.results;
            for (var i = 0; i < videos.length; i++) {
                videos[i].url = "https://www.youtube.com/embed/" + videos[i].key;
                videos[i].urlTrust = $sce.trustAsResourceUrl(videos[i].url);
            }
            $scope.videos = videos;
        });

    movieService.getCredits($routeParams.id)
        .success(function (results) {
            $scope.loadingActors = true;

            var actorIDs = [];
            actorLoop: for (var i = 0; i < results.cast.length; i++) {
                var obj = results.cast[i];
                if(obj === undefined) {
                    break;
                }

                for (var j = 0; j < actorIDs.length; j++) {
                    if(obj.id === actorIDs[j]) {
                        delete results.cast[i];
                        continue actorLoop;
                    }
                }
                actorIDs.push(obj.id);

                if(obj.profile_path) {
                    obj.image = base_url + "w150" + obj.profile_path;
                } else {
                    obj.image = "img/actor-default.png";
                }
            }

            var cast = [];
            for (var i = 0; i < results.cast.length; i++) {
                if(results.cast[i]) {
                    cast.push(results.cast[i]);
                }
            }
            $scope.cast = cast;

            var crewIDs = [];
            crewLoop: for (var i = 0; i < results.crew.length; i++) {
                var obj = results.crew[i];
                if(obj === undefined) {
                    break;
                }

                for (var j = 0; j < crewIDs.length; j++) {
                    if(obj.id === crewIDs[j]) {
                        delete results.crew[i];
                        continue crewLoop;
                    }
                }
                crewIDs.push(obj.id);

                if(obj.profile_path) {
                    obj.image = base_url + "w150" + obj.profile_path;
                } else {
                    obj.image = "img/actor-default.png";
                }
            }

            var crew = [];
            for (var i = 0; i < results.crew.length; i++) {
                if(results.crew[i]) {
                    crew.push(results.crew[i]);
                }
            }
            $scope.crew = crew;
        });

    $scope.moreActors = function () {
        $scope.actorLimit += 6;
    };

    $scope.moreCrews = function () {
        $scope.crewLimit += 4;
    };

    $scope.moreVideos = function () {
        $scope.videoLimit ++;
    };

    $scope.detailActor = function (id) {
        $location.path('/actor/'+id);
    }

}]);


app.controller('ActorDetailCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', 'movieService', '$routeParams', '$sce', function ($scope, $http, $q, $rootScope, $location, movieService, $routeParams, $sce) {

    var base_url = "http://image.tmdb.org/t/p/";

    $scope.loading = true;

    $scope.castsLimit = 6;

    $scope.crewsLimit = 4;

    movieService.getActor($routeParams.id)
        .success(function (result) {
            $scope.loading = false;

            if(result.profile_path) {
                result.poster = base_url + "w300" + result.profile_path;
            } else {
                result.poster = "img/actor-default.png";
            }

            $scope.actor = result;
        })
        .then(function () {
            $scope.loading = false;
        });

    movieService.getActorMovieCredits($routeParams.id)
        .success(function (result) {
            $scope.loadingMovies = true;

            var castsId = [];

            castLoop:for (var i = 0; i < result.cast.length; i++) {
                var obj = result.cast[i];

                for (var j = 0; j < castsId.length; j++) {
                    if (obj.id === castsId[j]) {
                        delete result.cast[i];
                        continue castLoop;
                    }
                }

                castsId.push(obj.id);
                if(obj.poster_path) {
                    obj.poster = base_url + "w300" + obj.poster_path;
                } else {
                    obj.poster = "img/movie-default.png";
                }
            }

            var cast = [];
            for (var i = 0; i < result.cast.length; i++) {
                if(result.cast[i]) {
                    cast.push(result.cast[i]);
                }
            }

            $scope.casts = cast;

            var crewsID = [];

            crewLoop:for (var i = 0; i < result.crew.length; i++) {
                obj = result.crew[i];

                for (var j = 0; j < crewsID.length; j++) {
                    if (obj.id === crewsID[j]) {
                        delete result.crew[i];
                        continue crewLoop;
                    }
                }

                crewsID.push(obj.id);
                if(obj.poster_path) {
                    obj.poster = base_url + "w300" + obj.poster_path;
                } else {
                    obj.poster = "img/movie-default.png";
                }
            }

            var crew = [];
            for (var i = 0; i < result.crew.length; i++) {
                if(result.crew[i]) {
                    crew.push(result.crew[i]);
                }
            }

            $scope.crews = crew;
        });

    $scope.moreCasts = function () {
        $scope.castsLimit += 6;
    };

    $scope.moreCrews = function () {
        $scope.crewsLimit += 4;
    };

    $scope.detailMovie = function (id) {
        $location.path('/movie/'+id);
    }

}]);
